<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_signUp</name>
   <tag></tag>
   <elementGuidId>4c51465b-fb17-4828-9d64-987c3e0c01e0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//a[contains(.,'Sign Up') and @href=&quot;https://www.phptravels.net/register&quot;])[3]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
