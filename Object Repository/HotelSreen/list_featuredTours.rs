<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>list_featuredTours</name>
   <tag></tag>
   <elementGuidId>fa3024f5-6f21-4fc2-9b74-174e04c574ff</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='body-section']/div[3]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>container</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>


 Featured Tours




  
    
      
        
        
        
      
      
        
          Spectaculars Of The Nile…
          
           Alexandria          
        
        
          
            
          
          
          6 Reviews          
        
        
        2 Days / 1 Nights        
        
          
            
              
                              
                  150
                  
                    USD                  
                
                              
            
            
              
                
                  Avg / Person                
              
            
          
          
            
            Book Now            
          
        
      
    
  
    
    
      
        
        
        
      
      
        
          6 Days Around Thailand
          
           Bangkok          
        
        
          
            
          
          
          2 Reviews          
        
        
        5 Days / 4 Nights        
        
          
            
              
                              
                  88
                  
                    USD                  
                
                              
            
            
              
                
                  Avg / Person                
              
            
          
          
            
            Book Now            
          
        
      
    
  
    
    
      
        
        
        
      
      
        
          Hong Kong Island Tour
          
           Hong Kong          
        
        
          
            
          
          
          0 Reviews          
        
        
        2 Days / 3 Nights        
        
          
            
              
                              
                  75
                  
                    USD                  
                
                              
            
            
              
                
                  Avg / Person                
              
            
          
          
            
            Book Now            
          
        
      
    
  
    
    
      
        
        
        
      
      
        
          Old and New Delhi City Tour
          
           Kirti Nagar          
        
        
          
            
          
          
          0 Reviews          
        
        
        8 Days / 6 Nights        
        
          
            
              
                              
                  100
                  
                    USD                  
                
                              
            
            
              
                
                  Avg / Person                
              
            
          
          
            
            Book Now            
          
        
      
    
  
    
    
      
        
        
        
      
      
        
          Sydney and Bondi Beach Explorer
          
           Tamarama          
        
        
          
            
          
          
          0 Reviews          
        
        
        5 Days / 4 Nights        
        
          
            
              
                              
                  125
                  
                    USD                  
                
                              
            
            
              
                
                  Avg / Person                
              
            
          
          
            
            Book Now            
          
        
      
    
  
    
    
      
        
        
        
      
      
        
          Legoland Malaysia Day Pass
          
           Legoland          
        
        
          
            
          
          
          0 Reviews          
        
        
        1 Days / 1 Nights        
        
          
            
              
                              
                  45
                  
                    USD                  
                
                              
            
            
              
                
                  Avg / Person                
              
            
          
          
            
            Book Now            
          
        
      
    
  
    
    
      
        
        
        
      
      
        
          Hurghada Sunset Desert Safari
          
           Hurghada          
        
        
          
            
          
          
          0 Reviews          
        
        
        4 Days / 5 Nights        
        
          
            
              
                              
                  50
                  
                    USD                  
                
                              
            
            
              
                
                  Avg / Person                
              
            
          
          
            
            Book Now            
          
        
      
    
  
    
    
      
        
        
        
      
      
        
          Day Visit of Petra from Oman
          
           Petra          
        
        
          
            
          
          
          0 Reviews          
        
        
        1 Days / 1 Nights        
        
          
            
              
                              
                  66
                  
                    USD                  
                
                              
            
            
              
                
                  Avg / Person                
              
            
          
          
            
            Book Now            
          
        
      
    
  
  


</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;body-section&quot;)/div[@class=&quot;container&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <value>//div[@id='body-section']/div[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Read More'])[3]/following::div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Peace Train A Long Time Coming'])[1]/following::div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <value>//div[5]/div[3]</value>
   </webElementXpaths>
</WebElementEntity>
