<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Header_ContactInfo</name>
   <tag></tag>
   <elementGuidId>566dc11a-e808-4489-bbfb-f3ba7af30ff1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//ul</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>ul</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>hidden-sm hidden-xs</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                
                            
                            Call Now: +1-234-56789
                        
                                                                        
                            |
                            
                            info@travelagency.com
                        
                                            </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;tbar-top hidden-sm hidden-xs&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-6 col-sm-4 col-xs-7 go-right&quot;]/div[@class=&quot;contact-box&quot;]/ul[@class=&quot;hidden-sm hidden-xs&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <value>//ul</value>
   </webElementXpaths>
</WebElementEntity>
