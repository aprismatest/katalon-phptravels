<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>List_Featured Cars</name>
   <tag></tag>
   <elementGuidId>1535c423-b3c0-4294-ac67-d7858ad44b7c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='body-section']/div[5]/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>container</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        
             
                 Featured Cars            
        
                    
                
                    
                        
                        
                        
                            
                                
                                    
                                        Book Now                                    
                                
                            
                        
                    
                    
                        
                        
                        
                                                        
                            USD $50                            
                                                    
                        
                        
                            Toyota Camry 2015 full option
                            
                                Muscat  
                            
                        
                    
                
            
                    
                
                    
                        
                        
                        
                            
                                
                                    
                                        Book Now                                    
                                
                            
                        
                    
                    
                        
                        
                        
                                                        
                            USD $99                            
                                                    
                        
                        
                            Opel Astra 2014
                            
                                Hoonani  
                            
                        
                    
                
            
                    
                
                    
                        
                        
                        
                            
                                
                                    
                                        Book Now                                    
                                
                            
                        
                    
                    
                        
                        
                        
                                                        
                            USD $75                            
                                                    
                        
                        
                            Ford Mondeo 2012
                            
                                Legoland  
                            
                        
                    
                
            
                    
                
                    
                        
                        
                        
                            
                                
                                    
                                        Book Now                                    
                                
                            
                        
                    
                    
                        
                        
                        
                                                        
                            USD $50                            
                                                    
                        
                        
                            Ford Focus 2014
                            
                                Kauai  
                            
                        
                    
                
            
            </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;body-section&quot;)/div[5]/div[@class=&quot;container&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <value>//div[@id='body-section']/div[5]/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='View More Offers'])[1]/following::div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Dubai Special Packages'])[1]/following::div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <value>//div[5]/div[5]/div</value>
   </webElementXpaths>
</WebElementEntity>
