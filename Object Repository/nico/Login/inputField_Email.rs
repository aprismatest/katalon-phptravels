<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Text Email</description>
   <name>inputField_Email</name>
   <tag></tag>
   <elementGuidId>00c50052-a165-4605-a1fc-adcb8ce2118f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@placeholder=&quot;Email&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
