<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Text Password</description>
   <name>inputField_Password</name>
   <tag></tag>
   <elementGuidId>a9440f76-dbad-4941-a530-0f3a8070d2f0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@placeholder=&quot;Password&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
