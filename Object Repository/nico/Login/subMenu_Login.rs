<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>subMenu_Login</description>
   <name>subMenu_Login</name>
   <tag></tag>
   <elementGuidId>b3d4edfd-8bfe-498a-a674-e44c7e73c543</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//a[contains(., 'Login')])[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
