<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Check Out</description>
   <name>inputField_check-out</name>
   <tag></tag>
   <elementGuidId>a7cd4364-880e-4eb1-9edb-b96a9f6f46b8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@placeholder=&quot;Check out&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
