<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Menu Hotel</description>
   <name>menu_Hotel</name>
   <tag></tag>
   <elementGuidId>c79fb826-22e5-44a4-b4fc-a0964882b6dd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//span[contains(.,'Hotels')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
