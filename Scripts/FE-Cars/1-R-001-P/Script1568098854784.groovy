import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser(GlobalVariable.web)

WebUI.navigateToUrl(GlobalVariable.web)

WebUI.verifyElementPresent(findTestObject('FE-Cars/1-R-001-P/button_cars'), 0)

WebUI.verifyElementPresent(findTestObject('FE-Cars/1-R-001-P/dropList_pickupLocation'), 0)

WebUI.verifyElementPresent(findTestObject('FE-Cars/1-R-001-P/dropList_dropoffLocation'), 0)

WebUI.verifyElementPresent(findTestObject('FE-Cars/1-R-001-P/datepickList_dateDepature'), 0)

WebUI.verifyElementPresent(findTestObject('FE-Cars/1-R-001-P/dropList_timeDeparture'), 0)

WebUI.verifyElementPresent(findTestObject('FE-Cars/1-R-001-P/datepickList_dropoffdate'), 0)

WebUI.verifyElementPresent(findTestObject('FE-Cars/1-R-001-P/dropList_dropofftime'), 0)

WebUI.verifyElementPresent(findTestObject('FE-Cars/1-R-001-P/button_search'), 0)

WebUI.closeBrowser()

