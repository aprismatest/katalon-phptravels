import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser(GlobalVariable.web)

WebUI.verifyElementPresent(findTestObject('HotelSreen/headerContact'), 0)

WebUI.verifyElementPresent(findTestObject('HotelSreen/logoPHPTravels'), 0)

WebUI.verifyElementPresent(findTestObject('HotelSreen/menu_Flights'), 0)

WebUI.verifyElementPresent(findTestObject('HotelSreen/menu_Hotels'), 0)

WebUI.verifyElementPresent(findTestObject('HotelSreen/menu_Tours'), 0)

WebUI.verifyElementPresent(findTestObject('HotelSreen/menu_Cars'), 0)

WebUI.verifyElementPresent(findTestObject('HotelSreen/MenuOffers'), 0)

WebUI.verifyElementPresent(findTestObject('HotelSreen/menuBlog'), 0)

WebUI.verifyElementPresent(findTestObject('HotelSreen/dorpDownList_ Account'), 0)

WebUI.verifyElementPresent(findTestObject('HotelSreen/dropDownList_currency'), 0)

WebUI.verifyElementPresent(findTestObject('HotelSreen/dropDownList_Language'), 0)

WebUI.verifyElementPresent(findTestObject('HotelSreen/SearchHotel'), 0)

WebUI.verifyElementPresent(findTestObject('HotelSreen/checkIn'), 0)

WebUI.verifyElementPresent(findTestObject('HotelSreen/checkOut'), 0)

WebUI.verifyElementPresent(findTestObject('HotelSreen/guest'), 0)

WebUI.verifyElementPresent(findTestObject('HotelSreen/buttonSearch'), 0)

WebUI.verifyElementPresent(findTestObject('HotelSreen/list_promotion'), 0)

WebUI.verifyElementPresent(findTestObject('HotelSreen/list_featuredTours'), 0)

WebUI.verifyElementPresent(findTestObject('HotelSreen/list_offer'), 0)

WebUI.verifyElementPresent(findTestObject('HotelSreen/list_featuredCars'), 0)

WebUI.verifyElementPresent(findTestObject('HotelSreen/list_GetMobileApp'), 0)

WebUI.verifyElementPresent(findTestObject('HotelSreen/list_contact'), 0)

WebUI.closeBrowser()

