import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.web)

WebUI.verifyElementPresent(findTestObject('HotelSreen/Hotel'), 0)

WebUI.verifyElementPresent(findTestObject('HotelSreen/SearchHotel'), 0)

WebUI.verifyElementPresent(findTestObject('HotelSreen/checkIn'), 0)

WebUI.verifyElementPresent(findTestObject('HotelSreen/checkOut'), 0)

WebUI.click(findTestObject('HotelSreen/SearchHotel'))

WebUI.setText(findTestObject('HotelSreen/inputHotel'), 'Batam')

WebUI.delay(2)

WebUI.sendKeys(findTestObject('HotelSreen/inputHotel'), Keys.chord(Keys.ENTER))

WebUI.delay(2)

WebUI.click(findTestObject('HotelSreen/checkIn'))

WebUI.delay(2)

WebUI.sendKeys(findTestObject('HotelSreen/checkIn'), Keys.chord('20/09/2019', Keys.TAB))

WebUI.delay(2)

WebUI.click(findTestObject('HotelSreen/checkOut'))

WebUI.delay(2)

WebUI.sendKeys(findTestObject('HotelSreen/checkOut'), Keys.chord('25/09/2019', Keys.TAB))

WebUI.delay(2)

WebUI.click(findTestObject('HotelSreen/guest'))

WebUI.click(findTestObject('HotelSreen/guestAdltMin'))

WebUI.click(findTestObject('HotelSreen/guestChildPlus'))

WebUI.click(findTestObject('HotelSreen/buttonSearch'))

WebUI.delay(2)

WebUI.click(findTestObject('detailHotelScreen/buttonDetail'))

WebUI.delay(2)

WebUI.scrollToElement(findTestObject('detailHotelScreen/buttonBook'), 0)

WebUI.verifyElementPresent(findTestObject('detailHotelScreen/buttonBook'), 0)

WebUI.executeJavaScript('', [])

WebUI.click(findTestObject('detailHotelScreen/buttonBook'))

