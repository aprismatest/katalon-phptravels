import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent
import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint

WebUI.openBrowser(GlobalVariable.FE_URL)

WebUI.navigateToUrl(GlobalVariable.FE_URL)

WebUI.verifyElementPresent(findTestObject('FE_Login/icon_phpTravels'), 0)

WebUI.verifyElementPresent(findTestObject('FE_Login/menu_hotels'), 0)

WebUI.verifyElementPresent(findTestObject('FE_Login/menu_flights'), 0)

WebUI.verifyElementPresent(findTestObject('FE_Login/menu_tours'), 0)

WebUI.verifyElementPresent(findTestObject('FE_Login/menu_cars'), 0)

WebUI.verifyElementPresent(findTestObject('FE_Login/menu_blog'), 0)

WebUI.verifyElementPresent(findTestObject('FE_Login/menu_offers'), 0)

WebUI.verifyElementPresent(findTestObject('FE_Login/dropdownlist_myAccount'), 0)

WebUI.verifyElementPresent(findTestObject('FE_Login/dropdownlist_currency'), 0)

WebUI.verifyElementPresent(findTestObject('FE_Login/dropdownlist_language'), 0)

WebUI.verifyElementPresent(findTestObject('FE_Login/icon_person'), 0)

WebUI.verifyElementPresent(findTestObject('FE_Login/label_email'), 0)

WebUI.verifyElementPresent(findTestObject('FE_Login/label_password'), 0)

WebUI.verifyElementPresent(findTestObject('FE_Login/inputField_email'), 0)

WebUI.verifyElementPresent(findTestObject('FE_Login/inputField_password'), 0)

WebUI.verifyElementPresent(findTestObject('FE_Login/button_login'), 0)

WebUI.verifyElementPresent(findTestObject('FE_Login/button_signUp'), 0)

WebUI.verifyElementPresent(findTestObject('FE_Login/button_forgetPassword'), 0)

WebUI.verifyElementPresent(findTestObject('FE_Login/header_login'), 0)

//CustomKeywords.'zoom.ZoomScreen.zoomOut'(5)
CustomKeywords.'get.ScreenCapture.takeScreenShotWithDelayAndName'('1-R-001-P-step1', 3, 5)

WebUI.closeBrowser()

