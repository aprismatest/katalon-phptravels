import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('https://www.phptravels.net/')

WebUI.click(findTestObject('Page_PHPTRAVELS  Travel Technology/Menu_Flights'))

WebUI.delay(2)

WebUI.click(findTestObject('Page_PHPTRAVELS  Travel Technology/origin'))

WebUI.setText(findTestObject('Page_PHPTRAVELS  Travel Technology/inputan bandara'), 'btj')

WebUI.click(findTestObject('Page_PHPTRAVELS  Travel Technology/pilihan bandara'))

WebUI.delay(2)

WebUI.click(findTestObject('Page_PHPTRAVELS  Travel Technology/Destination'))

WebUI.setText(findTestObject('Page_PHPTRAVELS  Travel Technology/inputan bandara'), 'cgk')

WebUI.click(findTestObject('Page_PHPTRAVELS  Travel Technology/pilihan bandara'))

WebUI.delay(2)

WebUI.click(findTestObject('Page_PHPTRAVELS  Travel Technology/Checklist_One Way'))

WebUI.click(findTestObject('Page_PHPTRAVELS  Travel Technology/Date_Departure'))

WebUI.delay(2)

WebUI.click(findTestObject('Page_PHPTRAVELS  Travel Technology/select_datedeparture'))

CustomKeywords.'utility.Click.ClickUsingJavaScript'(findTestObject('Page_PHPTRAVELS  Travel Technology/inputbox_Guest'), 
    0)

WebUI.delay(2)

CustomKeywords.'utility.Click.ClickUsingJavaScript'(findTestObject('Page_PHPTRAVELS  Travel Technology/inputbox_adult'), 
    0)

WebUI.click(findTestObject('Page_PHPTRAVELS  Travel Technology/Select_adultguest'))

WebUI.click(findTestObject('Page_PHPTRAVELS  Travel Technology/Select_childguest'))

WebUI.click(findTestObject('Page_PHPTRAVELS  Travel Technology/Select_infantguest'))

WebUI.click(findTestObject('Page_PHPTRAVELS  Travel Technology/button_Done'))

WebUI.delay(1)

WebUI.click(findTestObject('Page_PHPTRAVELS  Travel Technology/button_Search'))

WebUI.delay(5)

CustomKeywords.'get.ScreenCapture.takeScreenShotWithDelayAndName'('', 0, 0)

WebUI.closeBrowser()

