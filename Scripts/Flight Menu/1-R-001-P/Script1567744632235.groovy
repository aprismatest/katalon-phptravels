import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('https://www.phptravels.net/')

WebUI.click(findTestObject('Page_PHPTRAVELS  Travel Technology/Menu_Flights'))

WebUI.verifyElementPresent(findTestObject('Page_PHPTRAVELS  Travel Technology/Menu_Blog'), 0)

WebUI.verifyElementPresent(findTestObject('Page_PHPTRAVELS  Travel Technology/Menu_Cars'), 0)

WebUI.verifyElementPresent(findTestObject('Page_PHPTRAVELS  Travel Technology/Menu_Flights'), 0)

WebUI.verifyElementPresent(findTestObject('Page_PHPTRAVELS  Travel Technology/Menu_Hotels'), 0)

WebUI.verifyElementPresent(findTestObject('Page_PHPTRAVELS  Travel Technology/Menu_Offers'), 0)

WebUI.verifyElementPresent(findTestObject('Page_PHPTRAVELS  Travel Technology/Menu_Tours'), 0)

WebUI.verifyElementPresent(findTestObject('Page_PHPTRAVELS  Travel Technology/button_Search'), 0)

WebUI.verifyElementPresent(findTestObject('Page_PHPTRAVELS  Travel Technology/Checklist_One Way'), 0)

WebUI.verifyElementPresent(findTestObject('Page_PHPTRAVELS  Travel Technology/Checklist_Round Trip'), 0)

WebUI.verifyElementPresent(findTestObject('Page_PHPTRAVELS  Travel Technology/Date_Departure'), 0)

WebUI.verifyElementPresent(findTestObject('Page_PHPTRAVELS  Travel Technology/InputField_Destination'), 0)

WebUI.verifyElementPresent(findTestObject('Page_PHPTRAVELS  Travel Technology/Dropdownlist_Language'), 0)

WebUI.verifyElementPresent(findTestObject('Page_PHPTRAVELS  Travel Technology/Dropdownlist_listcurrency'), 0)

WebUI.verifyElementPresent(findTestObject('Page_PHPTRAVELS  Travel Technology/Dropdownlist_MyAccount'), 0)

WebUI.verifyElementPresent(findTestObject('Page_PHPTRAVELS  Travel Technology/Header_ContactInfo'), 0)

WebUI.verifyElementPresent(findTestObject('Page_PHPTRAVELS  Travel Technology/inputbox_Guest'), 0)

WebUI.verifyElementPresent(findTestObject('Page_PHPTRAVELS  Travel Technology/Link_Offers'), 0)

WebUI.verifyElementPresent(findTestObject('Page_PHPTRAVELS  Travel Technology/InputField_Departure'), 0)

CustomKeywords.'get.ScreenCapture.takeScreenShotWithDelayAndName'('', 0, 0)

WebUI.closeBrowser()

