import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('NicoTestCase/Login'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

WebUI.verifyElementPresent(findTestObject('Search Hotel/menu_Hotel'), 0)

WebUI.delay(3)

WebUI.click(findTestObject('Search Hotel/menu_Hotel'), FailureHandling.STOP_ON_FAILURE)

not_run: CustomKeywords.'utility.Click.ClickUsingJavaScript'(findTestObject('Page_My Account/a_Hotels'), 0)

WebUI.delay(3)

WebUI.click(findTestObject('Search Hotel/Field_HotelOrCityName'))

WebUI.sendKeys(findTestObject('Search Hotel/inputFieldHotelOrCityName'), 'alzer')

WebUI.click(findTestObject('Search Hotel/alzer_List'))

WebUI.click(findTestObject('Search Hotel/inputField_check_in'))

not_run: WebUI.delay(3)

not_run: CustomKeywords.'utility.Click.ClickUsingJavaScript'(findTestObject('Search Hotel/inputField_check_in'), 0)

WebUI.delay(3)

not_run: WebUI.click(findTestObject('Search Hotel/date_Check_in'))

CustomKeywords.'utility.Click.ClickUsingJavaScript'(findTestObject('Search Hotel/date_Check_in'), 0)

WebUI.delay(3)

WebUI.click(findTestObject('Search Hotel/inputField_check-out'))

WebUI.delay(3)

CustomKeywords.'utility.Click.ClickUsingJavaScript'(findTestObject('Search Hotel/Date_Check_Out'), 0)

WebUI.click(findTestObject('Search Hotel/passenger'))

WebUI.setText(findTestObject('Search Hotel/inputField_Adult'), '3')

WebUI.click(findTestObject('Search Hotel/button_Search'))

