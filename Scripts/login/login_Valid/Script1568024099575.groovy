import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.web)

WebUI.verifyElementPresent(findTestObject('login/dropdown_MyAccount'), 0)

WebUI.verifyElementPresent(findTestObject('login/click_Login'), 0)

WebUI.verifyElementPresent(findTestObject('login/click_SignUp'), 0)

WebUI.delay(2)

WebUI.click(findTestObject('login/dropdown_MyAccount'))

WebUI.delay(1)

WebUI.click(findTestObject('login/click_Login'))

WebUI.delay(1)

WebUI.click(findTestObject('login/inputField_Email'))

WebUI.setText(findTestObject('login/inputField_Email'), GlobalVariable.email)

WebUI.delay(1)

WebUI.click(findTestObject('login/inputField_Password'))

WebUI.setText(findTestObject('login/inputField_Password'), GlobalVariable.password)

WebUI.delay(1)

WebUI.scrollToElement(findTestObject('login/button_Login'), 0)

WebUI.click(findTestObject('login/button_Login'))

WebUI.delay(3)

