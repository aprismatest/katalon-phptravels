import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.verifyElementPresent(findTestObject('default_login/dropdown_Account'), 0)

WebUI.verifyElementPresent(findTestObject('default_login/dropdown_Currency'), 0)

WebUI.verifyElementPresent(findTestObject('default_login/dropdown_Language'), 0)

WebUI.verifyElementPresent(findTestObject('default_login/Date'), 0)

WebUI.verifyElementPresent(findTestObject('default_login/Time'), 0)

WebUI.verifyElementPresent(findTestObject('default_login/navigationBar_Offers'), 0)

WebUI.verifyElementPresent(findTestObject('default_login/navigationBar_Blog'), 0)

WebUI.verifyElementPresent(findTestObject('default_login/navigationBar_Cars'), 0)

WebUI.verifyElementPresent(findTestObject('default_login/navigationBar_Tours'), 0)

WebUI.verifyElementPresent(findTestObject('default_login/navigationBar_Flights'), 0)

WebUI.verifyElementPresent(findTestObject('default_login/navigationBar_Hotels'), 0)

WebUI.verifyElementPresent(findTestObject('default_login/logo_Tours'), 0)

WebUI.click(findTestObject('default_login/dropdown_Language'))

WebUI.delay(2)

WebUI.click(findTestObject('default_login/dropdown_Currency'))

WebUI.delay(2)

WebUI.click(findTestObject('default_login/dropdown_Account'))

WebUI.delay(2)

WebUI.click(findTestObject('default_login/navigationBar_Offers'))

WebUI.delay(2)

WebUI.click(findTestObject('default_login/dropdown_Account'))

WebUI.delay(1)

WebUI.click(findTestObject('default_login/click_Account'))

WebUI.delay(1)

WebUI.click(findTestObject('default_login/navigationBar_Blog'))

WebUI.delay(2)

WebUI.click(findTestObject('default_login/dropdown_Account'))

WebUI.delay(1)

WebUI.click(findTestObject('default_login/click_Account'))

WebUI.delay(1)

WebUI.click(findTestObject('default_login/navigationBar_Cars'))

WebUI.delay(2)

WebUI.click(findTestObject('default_login/dropdown_Account'))

WebUI.delay(1)

WebUI.click(findTestObject('default_login/click_Account'))

WebUI.delay(1)

WebUI.click(findTestObject('default_login/navigationBar_Tours'))

WebUI.delay(2)

WebUI.click(findTestObject('default_login/dropdown_Account'))

WebUI.delay(1)

WebUI.click(findTestObject('default_login/click_Account'))

WebUI.delay(1)

WebUI.click(findTestObject('default_login/navigationBar_Flights'))

WebUI.delay(2)

WebUI.click(findTestObject('default_login/dropdown_Account'))

WebUI.delay(1)

WebUI.click(findTestObject('default_login/click_Account'))

WebUI.delay(1)

WebUI.click(findTestObject('default_login/navigationBar_Hotels'))

WebUI.delay(2)

WebUI.click(findTestObject('default_login/dropdown_Account'))

WebUI.delay(1)

WebUI.click(findTestObject('default_login/click_Account'))

WebUI.delay(1)

WebUI.click(findTestObject('default_login/logo_Tours'))

WebUI.delay(2)

WebUI.click(findTestObject('default_login/dropdown_Account'))

WebUI.delay(1)

WebUI.click(findTestObject('default_login/click_Account'))

WebUI.delay(1)

WebUI.scrollToElement(findTestObject('default_login/menu_Profile'), 0)

WebUI.scrollToElement(findTestObject('default_login/menu_Whistlist'), 0)

WebUI.scrollToElement(findTestObject('default_login/menu_Newsletter'), 0)

WebUI.delay(1)

WebUI.click(findTestObject('default_login/menu_Profile'))

WebUI.delay(2)

WebUI.click(findTestObject('default_login/menu_Whistlist'))

WebUI.delay(2)

WebUI.click(findTestObject('default_login/menu_Newsletter'))

WebUI.delay(2)

WebUI.scrollToElement(findTestObject('default_login/menu_Bookings'), 0)

WebUI.click(findTestObject('default_login/menu_Bookings'))

WebUI.delay(2)

