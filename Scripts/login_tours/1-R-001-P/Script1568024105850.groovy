import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('login/login_Valid'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.takeScreenshot()

WebUI.verifyElementPresent(findTestObject('default_login/dropdown_Account'), 0)

WebUI.verifyElementPresent(findTestObject('default_login/dropdown_Currency'), 0)

WebUI.verifyElementPresent(findTestObject('default_login/dropdown_Language'), 0)

WebUI.verifyElementPresent(findTestObject('default_login/Date'), 0)

WebUI.verifyElementPresent(findTestObject('default_login/Time'), 0)

WebUI.verifyElementPresent(findTestObject('default_login/navigationBar_Offers'), 0)

WebUI.verifyElementPresent(findTestObject('default_login/navigationBar_Blog'), 0)

WebUI.verifyElementPresent(findTestObject('default_login/navigationBar_Cars'), 0)

WebUI.verifyElementPresent(findTestObject('default_login/navigationBar_Tours'), 0)

WebUI.verifyElementPresent(findTestObject('default_login/navigationBar_Flights'), 0)

WebUI.verifyElementPresent(findTestObject('default_login/navigationBar_Hotels'), 0)

WebUI.verifyElementPresent(findTestObject('default_login/logo_Tours'), 0)

WebUI.verifyElementPresent(findTestObject('default_login/menu_Bookings'), 0)

WebUI.scrollToElement(findTestObject('default_login/menu_Profile'), 0)

WebUI.verifyElementPresent(findTestObject('default_login/menu_Profile'), 0)

WebUI.scrollToElement(findTestObject('default_login/menu_Whistlist'), 0)

WebUI.verifyElementPresent(findTestObject('default_login/menu_Whistlist'), 0)

WebUI.scrollToElement(findTestObject('default_login/menu_Newsletter'), 0)

WebUI.verifyElementPresent(findTestObject('default_login/menu_Newsletter'), 0)

WebUI.scrollToElement(findTestObject('default_login/button_downloadIOS'), 0)

WebUI.verifyElementPresent(findTestObject('default_login/button_downloadIOS'), 0)

WebUI.scrollToElement(findTestObject('default_login/button_downloadAndroid'), 0)

WebUI.verifyElementPresent(findTestObject('default_login/button_downloadAndroid'), 0)

WebUI.scrollToElement(findTestObject('default_login/logo_Facebook'), 0)

WebUI.delay(1)

WebUI.verifyElementPresent(findTestObject('default_login/logo_Facebook'), 0)

WebUI.verifyElementPresent(findTestObject('default_login/logo_Twitter'), 0)

WebUI.verifyElementPresent(findTestObject('default_login/logo_Youtube'), 0)

WebUI.verifyElementPresent(findTestObject('default_login/logo_Gmail'), 0)

WebUI.verifyElementPresent(findTestObject('default_login/logo_Instagram'), 0)

WebUI.scrollToElement(findTestObject('footer/footer_PHPTravels'), 0)

WebUI.delay(1)

WebUI.verifyElementPresent(findTestObject('footer/footer_Contact'), 0)

WebUI.verifyElementPresent(findTestObject('footer/footer_AboutUs'), 0)

WebUI.verifyElementPresent(findTestObject('footer/footer_HowToBook'), 0)

WebUI.verifyElementPresent(findTestObject('footer/footer_BookingTips'), 0)

WebUI.verifyElementPresent(findTestObject('footer/footer_OurPartners'), 0)

WebUI.verifyElementPresent(findTestObject('footer/footer_PrivacyPolicy'), 0)

WebUI.verifyElementPresent(findTestObject('footer/footer_TermsOfUse'), 0)

WebUI.verifyElementPresent(findTestObject('footer/footer_FAQ'), 0)

WebUI.verifyElementPresent(findTestObject('footer/footer_SupplierLogin'), 0)

WebUI.verifyElementPresent(findTestObject('footer/footer_SupplierSignUp'), 0)

WebUI.verifyElementPresent(findTestObject('footer/footer_ExtranetLogin'), 0)

WebUI.verifyElementPresent(findTestObject('footer/footer_InputEmailNewsletterField'), 0)

WebUI.verifyElementPresent(findTestObject('footer/footer_Subscribe'), 0)

WebUI.verifyElementPresent(findTestObject('footer/footer_PHPTravels'), 0)

WebUI.delay(2)

WebUI.click(findTestObject('default_login/navigationBar_Tours'))

WebUI.delay(1)

WebUI.takeScreenshot()

WebUI.delay(2)

