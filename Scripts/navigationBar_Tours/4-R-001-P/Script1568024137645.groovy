import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.Keys as Keys

import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

TestObject to = new TestObject()

def objectSearch = to.addProperty('xpath', ConditionType.EQUALS, '//a/span[contains(.,\'Search by Listing or City Name\')]')

for (int i = 1; i <= findTestData('Field_city').getRowNumbers(); i++) {
	WebUI.delay(2)

	WebUI.click(objectSearch)
	
	WebUI.delay(1)

	WebUI.setText(findTestObject('tours/inputClick_city'), findTestData('Field_city').getValue(1, i))
	
	WebUI.delay(1)

	WebUI.sendKeys(findTestObject('tours/inputClick_city'), Keys.chord(Keys.ENTER))

	objectSearch = to.addProperty('xpath', ConditionType.EQUALS, ('//a/span[contains(.,\'' + findTestData('Field_city').getValue(
			1, i)) + '\')]')
}

