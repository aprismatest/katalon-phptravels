import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.web)

WebUI.delay(3)

WebUI.verifyElementPresent(findTestObject('null'), 0)

WebUI.click(findTestObject('null'))

WebUI.verifyElementPresent(findTestObject('tours/inputField_city'), 0)

WebUI.verifyElementPresent(findTestObject('tours/inputField_date'), 0)

WebUI.verifyElementPresent(findTestObject('tours/inputField_guest'), 0)

WebUI.verifyElementPresent(findTestObject('tours/inputField_tourType'), 0)

WebUI.verifyElementPresent(findTestObject('null'), 0)

//WebUI.click(findTestObject('tours/inputField_city'))
TestObject to = new TestObject()

def objectSearch = to.addProperty('xpath', ConditionType.EQUALS, '//a/span[contains(.,\'Search by Listing or City Name\')]')

for (int i = 1; i <= findTestData('Field_city').getRowNumbers(); i++) {
    WebUI.delay(2)

    WebUI.click(objectSearch)

    //WebUI.delay(2)
    WebUI.setText(findTestObject('tours/inputClick_city'), findTestData('Field_city').getValue(1, i))

    WebUI.sendKeys(findTestObject('tours/inputClick_city'), Keys.chord(Keys.ENTER))

    objectSearch = to.addProperty('xpath', ConditionType.EQUALS, ('//a/span[contains(.,\'' + findTestData('Field_city').getValue(
            1, i)) + '\')]')
}

not_run: WebUI.sendKeys(findTestObject('tours/inputClick_city'), Keys.chord(Keys.ENTER))

not_run: WebUI.click(findTestObject('tours/inputField_city'))

for (int i = 1; i <= 3; i++) {
    def objectClick_date = to.addProperty('xpath', ConditionType.EQUALS, findTestData('Data Files/Field_date').getObjectValue(
            'date', i).toString())

    WebUI.click(findTestObject('tours/inputField_date'))

    WebUI.delay(2)

    WebUI.click(objectClick_date)
}

WebUI.click(findTestObject('tours/inputField_guest'))

WebUI.click(findTestObject('tours/guestQuantity'))

WebUI.click(findTestObject('tours/inputField_tourType'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

