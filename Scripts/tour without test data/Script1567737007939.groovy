import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.web)

WebUI.delay(5)

WebUI.verifyElementPresent(findTestObject('null'), 0)

WebUI.click(findTestObject('null'))

WebUI.verifyElementPresent(findTestObject('tours/inputField_city'), 0)

WebUI.verifyElementPresent(findTestObject('tours/inputField_date'), 0)

WebUI.verifyElementPresent(findTestObject('tours/inputField_guest'), 0)

WebUI.verifyElementPresent(findTestObject('tours/inputField_tourType'), 0)

WebUI.verifyElementPresent(findTestObject('null'), 0)

WebUI.click(findTestObject('tours/inputField_city'))

WebUI.sendKeys(findTestObject('tours/inputClick_city'), Keys.chord('Dubai', Keys.TAB, '30/09/2019'))

WebUI.delay(1)

WebUI.click(findTestObject('tours/inputClick_date'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('tours/inputField_guest'))

WebUI.click(findTestObject('tours/guestQuantity'))

WebUI.click(findTestObject('tours/inputField_tourType'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('null'))

