import org.apache.commons.lang.SystemUtils

import com.kms.katalon.core.annotation.AfterTestCase
import com.kms.katalon.core.annotation.AfterTestSuite
import com.kms.katalon.core.annotation.BeforeTestCase
import com.kms.katalon.core.annotation.BeforeTestSuite
import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.context.TestCaseContext
import com.kms.katalon.core.context.TestSuiteContext
import com.kms.katalon.core.logging.KeywordLogger

class PostScript {
	
	@BeforeTestCase
	def sampleBeforeTestCase(TestCaseContext testCaseContext) {
	}

	@AfterTestCase
	def sampleAfterTestCase(TestCaseContext testCaseContext) {
	}
	
	@BeforeTestSuite
	def sampleBeforeTestSuite(TestSuiteContext testSuiteContext) {
	}
	
	/*@AfterTestSuite
	def sampleAfterTestSuite(TestSuiteContext testSuiteContext) {
		KeywordLogger logging = KeywordLogger.getInstance();
		String homeFolder = System.getProperty("user.dir");
		String reportFolder = logging.getLogFolderPath();
		
		String command;
		if(SystemUtils.IS_OS_WINDOWS){
			command = "cmd /c start java -jar \""+homeFolder+"\\Drivers\\component-katalon-aprisma-0.2.jar\" \""+homeFolder+"\" \""+reportFolder+"\" 3";
		}
		else if(SystemUtils.IS_OS_LINUX){
			command = "nohup java -jar \""+homeFolder+"/Drivers/component-katalon-aprisma-0.2.jar\" \""+homeFolder+"\" \""+reportFolder+"\" 3 2>/dev/null &"
		}
		else{
			command = "nohup java -jar \""+homeFolder+"/Drivers/component-katalon-aprisma-0.2.jar\" \""+homeFolder+"\" \""+reportFolder+"\" 3 2>/dev/null &"
		}
		println(command);
		command.execute();
	}*/
	
	@AfterTestSuite
	def sampleAfterTestSuite(TestSuiteContext testSuiteContext) {
		String reportFolder = RunConfiguration.getReportFolder();
		
		//KeywordLogger logging = KeywordLogger.getInstance();
		String homeFolder = System.getProperty("user.dir");
		//String reportFolder = logging.getLogFolderPath();
		
		KeywordLogger log = new KeywordLogger();
		log.logInfo(reportFolder);
		String command;
		
		if(SystemUtils.IS_OS_WINDOWS){
			command = "cmd /c start java -jar \""+homeFolder+"\\Drivers\\component-katalon-aprisma-0.2.jar\" \""+homeFolder+"\" \""+reportFolder+"\" 3";
		}
		else if(SystemUtils.IS_OS_LINUX){
			command = "nohup java -jar \""+homeFolder+"/Drivers/component-katalon-aprisma-0.2.jar\" \""+homeFolder+"\" \""+reportFolder+"\" 3 2>/dev/null &"
		}
		else{
			command = "nohup java -jar \""+homeFolder+"/Drivers/component-katalon-aprisma-0.2.jar\" \""+homeFolder+"\" \""+reportFolder+"\" 3 2>/dev/null &"
		}
		
		//KeywordLogger logger = new KeywordLogger();
		
		//logger.logInfo(command);
		println(command);
		//logger.logInfo(command);
		
		command.execute();
	}
}